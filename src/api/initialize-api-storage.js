export const userStorageKey = '__user__';
export const columnsStorageKey = '__columns__';

export const initializeApiStorage = () => {
  persistUser();
  persistColumns();
};

const persistUser = () => {
  const user = {
    name: 'John Doe',
    age: 20,
  };

  const isUserPersisted = localStorage.getItem(userStorageKey);
  if (!isUserPersisted) {
    localStorage.setItem(userStorageKey, JSON.stringify(user));
  }
};

const persistColumns = () => {
  const columns = [
    {
      id: 1,
      name: 'TODO',
      cards: [
        { id: 1, name: 'Покормить кота' },
        { id: 2, name: 'Купить хлеб' },
      ],
    },
    {
      id: 2,
      name: 'In progress',
      cards: [{ id: 3, name: 'Сдать лабу' }],
    },
    {
      id: 3,
      name: 'Done',
      cards: [
        { id: 4, name: 'Сходить на лекцию к Даниилу Игоревичу' },
        { id: 5, name: 'Начать верстать на React-е' },
      ],
    },
  ];

  const isColumnsPersisted = localStorage.getItem(columnsStorageKey);
  if (!isColumnsPersisted) {
    localStorage.setItem(columnsStorageKey, JSON.stringify(columns));
  }
};
