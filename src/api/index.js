import { initializeApiStorage } from './initialize-api-storage';
import {
  getColumnsFromStorage,
  getUserFromStorage,
  addColumnToStorage,
  removeColumnFromStorage,
  addCardToStorage,
  removeCardFromStorage,
} from './api-storage-service';
import { promisify } from './utils';

initializeApiStorage();

const getCurrentUser = async () => {
  const currentUser = getUserFromStorage();

  return promisify(currentUser);
};

const getColumns = async () => {
  const columns = getColumnsFromStorage();

  return promisify(columns);
};

const createColumn = async (columnName) => {
  const column = { id: Date.now(), name: columnName };

  addColumnToStorage(column);

  return promisify(column);
};

const deleteColumn = async (columnId) => {
  removeColumnFromStorage(columnId);

  return promisify(true);
};

const createCard = async (columnId, cardName) => {
  const card = { id: Date.now(), name: cardName };

  addCardToStorage(columnId, card);

  return promisify(card);
};

const deleteCard = async (cardId) => {
  removeCardFromStorage(cardId);

  return promisify(true);
};

export const api = {
  getCurrentUser,
  getColumns,
  createColumn,
  deleteColumn,
  createCard,
  deleteCard,
};
